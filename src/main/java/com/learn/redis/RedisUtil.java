package com.learn.redis;

/**
 * @Author: Holoong
 * @Date: 2020/10/18 12:36
 */

import java.net.URI;
import com.learn.RedisFactory;
import org.springframework.stereotype.Component;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;


@Component
public class RedisUtil {

    private static JedisPool jedisPool = null;


    private static JedisPool pool = null;
    public static JedisPool getInstance() {
        if (jedisPool == null) {
            synchronized (RedisFactory.class) {
                if (jedisPool == null) {
                    JedisPoolConfig config = new JedisPoolConfig();

                    config.setMaxTotal(8);
                    config.setMaxIdle(8);
                    config.setMinIdle(8);
                    config.setTestOnBorrow(true);
                    config.setTestOnReturn(true);
                    config.setTestWhileIdle(true);
                    config.setMaxWaitMillis(3000);
                    config.setMinEvictableIdleTimeMillis(60);
                    config.setTimeBetweenEvictionRunsMillis(30);
                    config.setBlockWhenExhausted(false);

                    URI uri = URI.create("redis://192.168.150.30:6379");

                    jedisPool = new JedisPool(config, uri, 2000, 2000);
                }
            }
        }
        return jedisPool;
    }

    public static void setVal(String key, String val) {
        final Jedis resource = getInstance().getResource();
        try {
            resource.set(key, val);
            resource.expire(key, 30*60);
        } finally {
            resource.close();
        }
    }

    public static String getVal(String key) {
        final Jedis resource = getInstance().getResource();
        try {
            return resource.get(key);
        } finally {
            resource.close();
        }
    }

    public static void deleteVal(String key) {
        final Jedis resource = getInstance().getResource();
        try {
            resource.del(key);
        } finally {
            resource.close();
        }
    }

}