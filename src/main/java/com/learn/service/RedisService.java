package com.learn.service;

import com.learn.entity.Student;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * @Author: Holoong
 * @Date: 2020/10/18 12:58
 */

public interface RedisService {
    Student findById(int sno);

}