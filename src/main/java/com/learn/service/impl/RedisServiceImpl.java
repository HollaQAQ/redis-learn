package com.learn.service.impl;

import com.learn.entity.Student;
import com.learn.mapper.StudentMapper;
import com.learn.redis.RedisUtil;
import com.learn.service.RedisService;
import com.alibaba.fastjson.JSONObject;
import org.springframework.stereotype.Service;


import javax.annotation.Resource;


@Service
public class RedisServiceImpl implements RedisService {

    @Resource
    private StudentMapper studentMapper;

    @Override
    public Student findById(int id) {
        String studentJson = RedisUtil.getVal(String.valueOf(id));
        if (studentJson != null && studentJson.length() > 0) {
            return JSONObject.parseObject(studentJson, Student.class);
        } else {
            Student student = studentMapper.findById(id);
            RedisUtil.setVal(String.valueOf(id), JSONObject.toJSONString(student));
            return student;
        }
    }
}