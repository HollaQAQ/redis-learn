package com.learn.mapper;

import com.learn.entity.Student;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * @Author: Holoong
 * @Date: 2020/10/15 20:09
 */
public interface StudentMapper {

    int insertStudent(Student student);

    Student findById(@Param("id") int id);

    int deleteById(@Param("id") int id);

    int updateAgeById(@Param("id") int id, @Param("age") int age);



}
