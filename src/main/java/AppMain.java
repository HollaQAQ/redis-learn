import com.learn.RedisFactory;
import org.springframework.boot.SpringApplication;
import redis.clients.jedis.Jedis;

/**
 * @Author: Holoong
 * @Date: 2020/10/18 11:48
 */
public class AppMain {

    public static void main(String[] args) {
        SpringApplication.run(AppMain.class, args);
    }

//    public static void main(String[] args) {
//        final Jedis resource = RedisFactory.getInstance().getResource();
//        try{
//            resource.set("test","test redis");
//        }finally {
//            resource.close();
//        }
//    }

//    public static void main(String[] args) {
//        final Jedis resource = RedisFactory.getInstance().getResource();
//        App app = new App();
//        app.testSet("test","test redis");
//        final String val = app.testGet("test");
//        System.out.println(val);
//    }
//
//    public void testSet(String key, String val) {
//        final Jedis resource = RedisFactory.getInstance().getResource();
//        try{
//            resource.set(key, val);
//    }finally {
//            resource.close();
//        }
//    }
//    public String testGet(String key) {
//        final Jedis resource = RedisFactory.getInstance().getResource();
//        try{
//            final String val = resource.get(key);
//            return val;
//        }finally {
//            resource.close();
//        }
//    }


}

